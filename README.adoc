= Advent of Code 2021

Code for solving puzzles from https://adventofcode.com/2021[Advent of Code 2021].

Files ending in `.pi` are http://picat-lang.org/[Picat] programs.
(Written for Picat version 3.2.)
